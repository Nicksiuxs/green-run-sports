import React, { useState } from "react";
import { useAuth } from "contexts/AuthContext";
import InputWithLabel from "components/InputWithLabel/InputWithLabel";

const SingUp: React.FC = () => {
  const { singUp, logout } = useAuth();

  const [user, setUser] = useState({ email: "", password: "" });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    singUp(user.email, user.password);
  };

  const handleOnChange = (e: React.FormEvent<HTMLInputElement>) => {
    setUser({ ...user, [e.currentTarget.name]: e.currentTarget.value });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <InputWithLabel
          id="email"
          type="email"
          text="User"
          handleOnChange={handleOnChange}
        />
        <InputWithLabel
          id="password"
          type="password"
          text="Password"
          handleOnChange={handleOnChange}
        />
        <button>Login</button>
      </form>
      <button onClick={() => logout()}>cerrar sesión</button>
    </>
  );
};

export default SingUp;
