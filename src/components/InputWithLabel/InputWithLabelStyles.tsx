import styled from "styled-components";

const Div = styled.div`
  border: 0.0625rem solid rgba(0, 0, 0, 0.06);
  border-radius: 1.125rem;
  padding: 0.75rem 1rem 0.75rem 1rem;
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: ${(props) => props.theme.backgroundInput};
  max-height: 4.1875rem;

  & input {
    border: none;
    font-size: 0.875rem;
    outline: 0;
    font-family: DM Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 1.125rem;
    background: none;
  }

  & label {
    font-family: DM Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 0.875rem;
    line-height: 122.02%;
    color: ${(props) => props.theme.fontColorLabel};
  }
`;

export default Div;
