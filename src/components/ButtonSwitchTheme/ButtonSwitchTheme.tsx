import React from "react";

import { useTheme } from "contexts/ThemeContext";

import Button from "./ButtonSwitchThemeStyles";

const ButtonSwitchTheme = () => {
  const { theme, setTheme } = useTheme();
  const handleOnClick = () => {
    setTheme(theme === "light" ? "dark" : "light");
  };
  return (
    <Button onClick={handleOnClick}>{theme === "light" ? "🌙" : "🌤️"}</Button>
  );
};

export default ButtonSwitchTheme;
