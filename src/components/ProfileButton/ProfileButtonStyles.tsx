import styled from "styled-components";

const ButtonProfile = styled.button`
  border-radius: 100%;
  height: 2.125rem;
  width: 2.125rem;
  border: none;

  & img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 100%;
  }
`;

export default ButtonProfile;
