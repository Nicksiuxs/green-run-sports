import React from "react";
import { useNavigate } from "react-router";

import ButtonProfile from "./ProfileButtonStyles";

type props = {
  img: string;
};

const ProfileButton: React.FC<props> = ({ img }) => {
  const navigate = useNavigate();
  return (
    <ButtonProfile onClick={() => navigate("/profile")}>
      <img src={img} alt="profile" />
    </ButtonProfile>
  );
};

export default ProfileButton;
