import styled from "styled-components";

const Navbar = styled.nav`
  position: absolute;
  width: 100%;
  bottom: 0;
  z-index: 5;
  padding: 0 22px 20px 21px;

  & div {
    display: flex;
    justify-content: space-between;
    max-width: 100%;
    align-items: center;
    padding: 0.8125rem 1.375rem 0.8125rem 0.75rem;
    border-radius: 1.5rem;
    background-color: ${(props) => props.theme.navbarBackground};
  }
`;

export default Navbar;
