import React from "react";

import { ReactComponent as Menu } from "assets/icons/menu.svg";
import { ReactComponent as Watch } from "assets/icons/watch.svg";
import { ReactComponent as Notes } from "assets/icons/notes.svg";
import Profile from "assets/images/profile.png";

import NavButton from "components/NavButton/NavButton";

import Navbar from "./NavBarStyles";
import ProfileButton from "components/ProfileButton/ProfileButton";

const NavBar = () => {
  return (
    <Navbar>
      <div>
        <NavButton route="/" icon={<Menu />} />
        <NavButton route="/history" icon={<Watch />} />
        <NavButton route="/other" icon={<Notes />} />
        <ProfileButton img={Profile} />
      </div>
    </Navbar>
  );
};

export default NavBar;
