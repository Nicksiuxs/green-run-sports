import React from "react";

import ButtonSwitchTheme from "components/ButtonSwitchTheme/ButtonSwitchTheme";

import HeaderTools from "./HeaderStyles";
import { ButtonAlternative } from "components/ButtonSwitchTheme/ButtonSwitchThemeStyles";

const Header: React.FC = () => {
  return (
    <HeaderTools>
      <ButtonSwitchTheme />
      <ButtonAlternative>
        <span role="img" aria-label="Soccer">
          ⚽️
        </span>
      </ButtonAlternative>
    </HeaderTools>
  );
};

export default Header;
