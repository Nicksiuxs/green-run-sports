import React from "react";
import { useNavigate } from "react-router";

import ButtonLink from "./ButtonWithLinkStyles";

type ButtonLink = {
  text: string;
  link: string;
};

const ButtonWithLink: React.FC<ButtonLink> = ({ text, link }) => {
  const navigate = useNavigate();
  return <ButtonLink onClick={() => navigate(link)}>{text}</ButtonLink>;
};

export default ButtonWithLink;
