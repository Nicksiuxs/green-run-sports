import styled from "styled-components";

const ButtonLink = styled.button`
  font-family: "DM Sans", sans-serif;
  font-size: 1.125rem;
  color: #fefefe;

  border: none;
  border-radius: 1.5625rem;

  background: linear-gradient(99deg, #236bfe 6.69%, #0d4ed3 80.95%);
  box-shadow: 0rem 0.25rem 1.875rem rgba(34, 105, 251, 0.8);

  padding: 1.375rem 2.375rem;

  cursor: pointer;
`;

export default ButtonLink;
