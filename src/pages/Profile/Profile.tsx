import React from "react";
import ButtonLink from "components/ButtonWithLink/ButtonWithLinkStyles";
import { useAuth } from "contexts/AuthContext";

const Profile = () => {
  const { logout } = useAuth();
  return (
    <div>
      Profile
      <ButtonLink onClick={() => logout()}>Cerrar sesion</ButtonLink>
    </div>
  );
};

export default Profile;
