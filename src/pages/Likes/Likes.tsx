import React from "react";
import Header from "components/Header/Header";

import LikesBackground from "./LikesStyles";

const Likes = () => {
  return (
    <LikesBackground>
      <Header />
    </LikesBackground>
  );
};

export default Likes;
