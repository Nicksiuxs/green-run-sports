import React from "react";
import { HomeBackground, TextInfo } from "./HomeStyles";

import ButtonWithLink from "components/ButtonWithLink/ButtonWithLink";

import img from "assets/images/messi.png";

const Home = () => {
  return (
    <HomeBackground>
      <figure>
        <img src={img} alt="messi" />
      </figure>
      <TextInfo>
        <h1>Discover Your Best Sport With Us</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <ButtonWithLink text="Login" link="login" />
      </TextInfo>
    </HomeBackground>
  );
};

export default Home;
