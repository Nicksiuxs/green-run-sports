import styled from "styled-components";

const HomeBackground = styled.div`
  background-color: ${(props) => props.theme.backgrounButtonSwitch};
  height: 100vh;
  width: 100vw;
  position: relative;
  padding-top: 2.1875rem;

  & figure {
    max-width: 100%;
    max-height: 100%;
    transform: translate(-6.875rem, 0.5rem);
  }
`;

const TextInfo = styled.div`
  position: absolute;
  bottom: 0;
  z-index: 3;
  background-color: ${(props) => props.theme.backgoundTextContainer};
  border-radius: 2.25rem;
  padding: 2.875rem 2rem 2.875rem 2rem;

  & h1 {
    font-family: DM Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 28px;
    color: ${(props) => props.theme.fontColorTitle};
    margin-bottom: 0.75rem;
  }

  & p {
    color: ${(props) => props.theme.fontColorText};
    font-family: Epilogue;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 148.02%;
    margin-bottom: 3.375rem;
  }
`;

export { HomeBackground, TextInfo };
