import styled from "styled-components";

const LoginBackground = styled.div`
  background-color: ${(props) => props.theme.backgrounButtonSwitch};
  width: 100vw;
  height: 100vh;
  max-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0 1.9375rem 0 1.875rem;

  & h1 {
    font-family: DM Sans;
    font-style: normal;
    font-weight: bold;
    font-size: 2.625rem;
    color: ${(props) => props.theme.fontColorTitle};
    text-align: center;
    margin-bottom: 0.4375rem;
  }

  & p {
    font-family: Epilogue;
    font-style: normal;
    font-weight: normal;
    font-size: 1.125rem;
    text-align: center;
    line-height: 148.02%;
    margin-bottom: 1.4375rem;
  }

  & form {
    width: 100%;
    display: flex;
    flex-direction: column;

    & div {
      margin-bottom: 0.625rem;
    }

    & a {
      align-self: flex-start;
      text-decoration: none;
      font-family: DM Sans;
      font-style: normal;
      font-weight: normal;
      font-size: 1rem;
      line-height: 122.02%;
      margin: 0.625rem 0 1.375rem 0;
      color: ${(props) => props.theme.fontColorText};
    }

    & button {
      max-width: 7.625rem;
    }
  }
`;

export default LoginBackground;
