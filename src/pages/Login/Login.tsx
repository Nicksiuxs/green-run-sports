import React, { useState } from "react";
import { Link } from "react-router-dom";

import { useAuth } from "contexts/AuthContext";

import InputWithLabel from "components/InputWithLabel/InputWithLabel";

import LoginBackground from "./LoginStyles";
import ButtonLink from "components/ButtonWithLink/ButtonWithLinkStyles";

const Login: React.FC = () => {
  const { login } = useAuth();

  const [user, setUser] = useState({ email: "", password: "" });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (user.email !== "" && user.password !== "") {
      login(user.email, user.password);
    }
  };

  const handleOnChange = (e: React.FormEvent<HTMLInputElement>) => {
    setUser({ ...user, [e.currentTarget.name]: e.currentTarget.value });
  };

  return (
    <LoginBackground>
      <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      <form onSubmit={handleSubmit}>
        <InputWithLabel
          id="email"
          type="email"
          text="User"
          handleOnChange={handleOnChange}
        />
        <InputWithLabel
          id="password"
          type="password"
          text="Password"
          handleOnChange={handleOnChange}
        />
        <Link to="/reset-password">Forgot your password?</Link>
        <ButtonLink>Login</ButtonLink>
      </form>
    </LoginBackground>
  );
};

export default Login;
