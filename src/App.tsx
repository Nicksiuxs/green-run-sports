import React from "react";

import { useAuth } from "contexts/AuthContext";

import AuthRouter from "routers/AuthRouter";
import UnAuthRouter from "routers/UnAuthRouter";

function App() {
  const { currentUser } = useAuth();
  return currentUser ? <AuthRouter /> : <UnAuthRouter />;
}

export default App;
