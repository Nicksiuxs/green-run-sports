import React from "react";
import { Route, Routes, Navigate } from "react-router";

import Home from "pages/Home/Home";
import Login from "pages/Login/Login";

const UnAuthRouter = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/login" element={<Login />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default UnAuthRouter;
