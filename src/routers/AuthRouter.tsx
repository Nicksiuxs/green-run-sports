import React from "react";
import { Route, Routes, Navigate } from "react-router";

import Likes from "pages/Likes/Likes";
import Profile from "pages/Profile/Profile";
import History from "pages/History/History";

import NavBar from "components/NavBar/NavBar";

const AuthRouter = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Likes />} />
        <Route path="/history" element={<History />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
      <NavBar />
    </>
  );
};

export default AuthRouter;
