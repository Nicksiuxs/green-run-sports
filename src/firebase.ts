import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const appConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_API_KEY,
  appId: process.env.REACT_APP_FIREBASE_API_ID,
};

const firebaseApp = initializeApp(appConfig);

export const auth = getAuth(firebaseApp);
export default firebaseApp;
