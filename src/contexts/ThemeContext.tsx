import React, { useState, useContext, createContext } from "react";
import { ThemeProvider as ThemeProviderStyled } from "styled-components";

const LightTheme = {
  pageBackground: "#F3F3F3",
  fontColorTitle: "#161617",
  fontColorText: "#232232",
  backgrounButtonSwitch: "#FFFFFF",
  backgoundTextContainer: "#FFFFFF",
  fontColorLabel: "#3C3C3C",
  backgroundInput: "#FFFFFF",
  backgroundNavButton: "#FBFBFB",
  navbarBackground: "#FFFFFF",
  iconColor: "#EDEDED",
  iconActive: "#1A5BE1",
};

const DarkTheme = {
  pageBackground: "#181828",
  fontColorTitle: "#FEFEFE",
  fontColorText: "#FEFEFE",
  backgrounButtonSwitch: "#222243",
  backgoundTextContainer: "#2C2B3E",
  fontColorLabel: "#FEFEFE",
  backgroundInput: "##2F2F43",
  backgroundNavButton: "#1F1F31",
  navbarBackground: "#2C2B3E",
  iconColor: "#181828",
  iconActive: "#FFFFFF",
};

const themes: any = {
  light: LightTheme,
  dark: DarkTheme,
};

const ThemeContext = createContext({});

const useTheme: any = () => {
  return useContext(ThemeContext);
};

const ThemeProvider: React.FC = ({ children }) => {
  const [theme, setTheme] = useState("light");
  return (
    <ThemeContext.Provider value={{ theme, setTheme, themes }}>
      <ThemeProviderStyled theme={themes[theme]}>
        {children}
      </ThemeProviderStyled>
    </ThemeContext.Provider>
  );
};

export { useTheme };
export default ThemeProvider;
