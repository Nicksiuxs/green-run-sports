import React, { useState, useEffect, useContext, createContext } from "react";
import { auth } from "firebase";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";

interface Aux {
  currentUser: { email: string; password: string };
  singUp: (_email: string, _password: string) => any;
  login: (_email: string, _password: string) => any;
  logout: () => any;
}

const AuthContext = createContext({});

const useAuth: any = () => {
  return useContext(AuthContext);
};

const AuthProvider: React.FC = ({ children }) => {
  const [currentUser, setCurrentUser] = useState<null | any>(null);

  function singUp(email: string, password: string) {
    return createUserWithEmailAndPassword(auth, email, password);
  }

  function login(email: string, password: string) {
    return signInWithEmailAndPassword(auth, email, password);
  }

  function logout() {
    signOut(auth);
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
      console.log(user);
    });

    return unsubscribe;
  }, []);

  const data: Aux = { currentUser, singUp, login, logout };
  return <AuthContext.Provider value={data}>{children}</AuthContext.Provider>;
};

export { useAuth };
export default AuthProvider;
