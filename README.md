# GreenRun - Frontend Developer Test

This is the repository of the frontend developer test for GreenRun

## Deployment

[Link deploy](https://greenrun.netlify.app)

## Resources

[API Suggestion](https://www.thesportsdb.com/api.php)

[Desing](https://www.figma.com/file/1MHzP2V3Bvdrv9U0dH4Vl4/Prueba-Front-%2F-Greenrun?node-id=0%3A1)

### Login credentials

email: nicolas.londono@correo.com
password: 1234567
